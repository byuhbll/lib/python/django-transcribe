from xml.etree import ElementTree

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from transcribe.models import (
    FINISHED,
    REVIEW,
    SKIPPED,
    TRANSCRIPTION,
    Project,
    Task,
    TranscribeUser,
    UserTask,
)

User = get_user_model()


class TestProjectModel(TestCase):
    def setUp(self):
        self.transcribe_user = User.objects.create(
            username='tester'
        ).transcribe_user
        self.project = Project.objects.create(
            title='test', allow_global_transcriptions=True
        )
        self.a_task = Task.objects.create(project=self.project)

    def tearDown(self):
        User.objects.all().delete()
        TranscribeUser.objects.all().delete()
        Project.objects.all().delete()
        Task.objects.all().delete()
        UserTask.objects.all().delete()

    def test_init_no_priority_should_default_to_zero(self):
        project = Project()
        project.save()
        self.assertEqual(project.priority, 0)

    def test_delete(self):
        reviewer, _ = Group.objects.get_or_create(name='Reviewer')
        self.transcribe_user.user.groups.add(reviewer)
        UserTask.objects.create(
            task=self.a_task, status='finished', user=self.transcribe_user
        )
        UserTask.objects.create(
            task=self.a_task, status='finished', user=self.transcribe_user
        )
        UserTask.objects.create(
            task=self.a_task,
            task_type='review',
            status='finished',
            user=self.transcribe_user,
        )
        self.assertEqual(Project.objects.count(), 1)
        self.assertEqual(self.transcribe_user.num_finished_transcriptions, 2)
        self.assertEqual(self.transcribe_user.num_finished_reviews, 1)
        self.project.delete()
        self.assertEqual(Project.objects.count(), 0)
        self.assertEqual(Project.objects.archived().count(), 1)
        self.assertEqual(Project.objects.everything().count(), 1)
        self.assertEqual(self.transcribe_user.num_finished_transcriptions, 2)
        self.assertEqual(self.transcribe_user.num_finished_reviews, 1)

    def test_pending_transcription_task(self):
        usertask = UserTask.objects.create(
            task=self.a_task, user=self.transcribe_user, status='in progress'
        )
        self.assertEqual(
            self.project.pending_transcription_task(self.transcribe_user).pk,
            usertask.pk,
        )

    def test_no_pending_transcription_task(self):
        transcribe_user2 = User.objects.create(
            username='tester2'
        ).transcribe_user
        UserTask.objects.create(
            task=self.a_task, user=transcribe_user2, status='in progress'
        )
        self.assertIsNone(
            self.project.pending_transcription_task(self.transcribe_user)
        )

    def test_pending_review_task(self):
        usertask = UserTask.objects.create(
            task=self.a_task,
            user=self.transcribe_user,
            status='in progress',
            task_type='review',
        )
        self.assertEqual(
            self.project.pending_review_task(self.transcribe_user).pk,
            usertask.pk,
        )

    def test_no_pending_review_task(self):
        transcribe_user2 = User.objects.create(
            username='tester2'
        ).transcribe_user
        UserTask.objects.create(
            task=self.a_task,
            user=transcribe_user2,
            status='in progress',
            task_type='review',
        )
        self.assertIsNone(
            self.project.pending_review_task(self.transcribe_user)
        )

    def test_available_transcription_task(self):
        self.assertEqual(
            self.project.available_transcription_task(self.transcribe_user),
            self.a_task,
        )

    def test_no_available_transcription_task(self):
        # user already skipped the task
        ut = UserTask.objects.create(
            task=self.a_task, user=self.transcribe_user, status='skipped'
        )
        returned = self.project.available_transcription_task(
            self.transcribe_user
        )
        self.assertIsNone(returned)

        # user is already working on the task
        ut.status = 'in progress'
        ut.save()
        returned = self.project.available_transcription_task(
            self.transcribe_user
        )
        self.assertIsNotNone(returned)

        # user is already finished the task
        ut.status = 'finished'
        ut.save()
        returned = self.project.available_transcription_task(
            self.transcribe_user
        )
        self.assertIsNone(returned)

    def test_1_skipped_user_task_1_finished_task_should_be_available(self):
        project = Project(
            title='test',
            description='description',
            allow_global_transcriptions=True,
        )
        project.save()
        file = SimpleUploadedFile('afile.jpg', b'any bytes...')
        task = Task(project=project, file=file)
        task.save()
        user1 = User.objects.create(username='user1').transcribe_user
        user2 = User.objects.create(username='user2').transcribe_user
        user3 = User.objects.create(username='user3').transcribe_user
        UserTask(task=task, user=user1, status=SKIPPED).save()
        UserTask(task=task, user=user2, status=FINISHED).save()
        available_task = project.available_transcription_task(user3)
        self.assertEqual(available_task, task)

    def test_available_review_task(self):
        reviewer, _ = Group.objects.get_or_create(name='Reviewer')
        self.transcribe_user.user.groups.add(reviewer)

        UserTask.objects.create(
            task=self.a_task, user=self.transcribe_user, status='finished'
        )
        UserTask.objects.create(
            task=self.a_task, user=self.transcribe_user, status='finished'
        )

        self.assertEqual(
            self.project.available_review_task(self.transcribe_user),
            self.a_task,
        )

    def test_no_available_review_task(self):
        reviewer, _ = Group.objects.get_or_create(name='Reviewer')
        self.transcribe_user.user.groups.add(reviewer)
        transcribe_user2 = User.objects.create(
            username='tester2'
        ).transcribe_user
        transcribe_user2.user.groups.add(reviewer)

        # no user tasks completed
        returned = self.project.available_review_task(self.transcribe_user)
        self.assertIsNone(returned)

        # no finished user tasks
        ut1 = UserTask.objects.create(
            task=self.a_task, user=self.transcribe_user, status='in progress'
        )
        ut2 = UserTask.objects.create(
            task=self.a_task, user=self.transcribe_user, status='in progress'
        )

        returned = self.project.available_review_task(self.transcribe_user)
        self.assertIsNone(returned)

        # only one finished user task
        ut1.status = 'finished'
        ut1.save()
        returned = self.project.available_review_task(self.transcribe_user)
        self.assertIsNone(returned)

        # only one finished with a skipped
        ut2.status = 'skipped'
        ut2.save()
        returned = self.project.available_review_task(self.transcribe_user)
        self.assertIsNone(returned)

        # other user already reviewing
        ut2.status = 'finished'
        ut2.save()
        review_task = self.project.available_review_task(transcribe_user2)
        review_task.claim_review(transcribe_user2)
        self.assertEqual(review_task, self.a_task)
        review_ut = review_task.usertasks.filter(task_type='review').first()
        self.assertEqual(review_ut.status, 'in progress')

        # other user already finished review
        review_ut.status = 'finished'
        review_ut.save()

        returned = self.project.available_review_task(self.transcribe_user)
        self.assertIsNone(returned)

        # already skipped review
        review_ut.status = 'skipped'
        review_ut.user = self.transcribe_user
        review_ut.save()

        returned = self.project.available_review_task(self.transcribe_user)
        self.assertIsNone(returned)


class ProjectModelTranscriptGenerationTests(TestCase):
    def test_generate_txt_should_only_have_one_percent_sign(self):
        project = Project()
        project.save()
        output = project.generate_txt(False)
        expected = '\nMedia Type: text\nNumber of Tasks: 0\n0% complete'
        self.assertEqual(output, expected)

    def test_generate_txt_ampersand_escaped_should_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&amp;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_txt(False)
        expected = '\n&'
        self.assertTrue(output.endswith(expected))

    def test_generate_txt_lessthan_escaped_should_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_txt(False)
        expected = '\n<'
        self.assertTrue(output.endswith(expected))

    def test_generate_txt_greaterthan_escaped_should_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_txt(False)
        expected = '\n>'
        self.assertTrue(output.endswith(expected))

    def test_generate_txt_unescaped_tags_should_not_display(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '<b style="hey">Hello</b>'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_txt(False)
        expected = '\nHello'
        self.assertTrue(output.endswith(expected))

    def test_generate_txt_escaped_tags_should_display(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;b style="hey"&gt;Hello&lt;/b&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_txt(False)
        expected = '<b style="hey">Hello</b>'
        self.assertTrue(output.endswith(expected))

    def test_generate_xml_ampersand_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&amp;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '\n&amp;'
        self.assertTrue(expected in output)

    def test_generate_xml_lessthan_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '\n&lt;\n'
        self.assertTrue(expected in output)

    def test_generate_xml_greaterthan_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '\n&gt;\n'
        self.assertTrue(expected in output)

    def test_generate_xml_tag_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;b height="100px"&gt;hi.&lt;/b&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '&lt;b height="100px"&gt;hi.&lt;/b&gt;'
        self.assertTrue(expected in output)

    def test_generate_xml_ampersand_unescaped_should_not_escape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '\n&\n'
        self.assertTrue(expected in output)

    def test_generate_xml_lessthan_unescaped_should_not_escape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '<'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '\n<\n'
        self.assertTrue(expected in output)

    def test_generate_xml_greaterthan_unescaped_should_not_escape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '>'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '\n>\n'
        self.assertTrue(expected in output)

    def test_generate_xml_tag_unescaped_should_not_escape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '<b height="100px">hi.</b>'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_xml()
        expected = '<b height="100px">hi.</b>'
        self.assertTrue(expected in output)

    def test_generate_html_ampersand_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&amp;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_html()
        expected = (
            '<html>   <head>      <meta http-equiv="Content-Type" '
            'content="text/html; charset=UTF-8"><style>'
            'span.name{color: blue;}span.place{color: green;}'
            'span.date{color: orange;}span.page{color: red;}</style>'
            '</head>   <body><br><h1>                  '
            '               </h1><br> <br> <br> <br> <br> <br> <br> <br>'
            '&amp;<br> <br> <br> <br> <br> <br></body></html>'
        )
        self.assertHTMLEqual(output, expected)

    def test_generate_html_lessthan_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_html()
        expected = (
            '<html>   <head>      <meta http-equiv="Content-Type" '
            'content="text/html; charset=UTF-8"><style>'
            'span.name{color: blue;}span.place{color: green;}'
            'span.date{color: orange;}span.page{color: red;}</style>'
            '</head>   <body><br><h1>                  '
            '               </h1><br> <br> <br> <br> <br> <br> <br> <br>'
            '&lt;<br> <br> <br> <br> <br> <br></body></html>'
        )
        self.assertHTMLEqual(output, expected)

    def test_generate_html_greaterthan_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_html()
        expected = (
            '<html>   <head>      <meta http-equiv="Content-Type" '
            'content="text/html; charset=UTF-8"><style>'
            'span.name{color: blue;}span.place{color: green;}'
            'span.date{color: orange;}span.page{color: red;}</style>'
            '</head>   <body><br><h1>                  '
            '               </h1><br> <br> <br> <br> <br> <br> <br> <br>'
            '&gt;<br> <br> <br> <br> <br> <br></body></html>'
        )
        self.assertHTMLEqual(output, expected)

    def test_generate_html_tag_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;b height="100px"&gt;hi.&lt;/b&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_html()
        expected = (
            '<html>   <head>      <meta http-equiv="Content-Type" '
            'content="text/html; charset=UTF-8"><style>'
            'span.name{color: blue;}span.place{color: green;}'
            'span.date{color: orange;}span.page{color: red;}</style>'
            '</head>   <body><br><h1>                  '
            '               </h1><br> <br> <br> <br> <br> <br> <br> <br>'
            '&lt;b height="100px"&gt;hi.&lt;/b&gt;<br> <br> <br> <br> '
            '<br> <br></body></html>'
        )
        self.assertHTMLEqual(output, expected)

    def test_generate_html_xsl_tag_escaped_should_not_unescape(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '&lt;hi rend="bold"&gt;hi.&lt;/hi&gt;'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_html()
        expected = (
            '<html>   <head>      <meta http-equiv="Content-Type" '
            'content="text/html; charset=UTF-8"><style>'
            'span.name{color: blue;}span.place{color: green;}'
            'span.date{color: orange;}span.page{color: red;}</style>'
            '</head>   <body><br><h1>                  '
            '               </h1><br> <br> <br> <br> <br> <br> <br> <br>'
            '&lt;hi rend="bold"&gt;hi.&lt;/hi&gt;<br> <br> <br> <br> '
            '<br> <br></body></html>'
        )
        self.assertHTMLEqual(output, expected)

    def test_generate_html_xsl_tag_unescaped_should_render(self):
        project = Project()
        project.save()
        task = Task(project=project)
        task.save()
        transcribe_user = User.objects.create().transcribe_user
        user_task = UserTask(task=task, user=transcribe_user)
        user_task.transcription = '<hi rend="bold">hi.</hi>'
        user_task.task_type = REVIEW
        user_task.status = FINISHED
        user_task.save()
        output = project.generate_html()
        expected = (
            '<html>   <head>      <meta http-equiv="Content-Type" '
            'content="text/html; charset=UTF-8"><style>'
            'span.name{color: blue;}span.place{color: green;}'
            'span.date{color: orange;}span.page{color: red;}</style>'
            '</head>   <body><br><h1>                  '
            '               </h1><br> <br> <br> <br> <br> <br> <br> <br>'
            '<b>hi.</b><br> <br> <br> <br> '
            '<br> <br></body></html>'
        )
        self.assertHTMLEqual(output, expected)


class TestTaskModel(TestCase):

    """Tests for the task model."""

    def test_claim_review_should_enforce_transcribers_per_task(self):
        """
        When a task has more transcriptions than its project's
        transcribers_per_task, it should only use as many transcriptions
        as that project is supposed to have.
        """
        p = Project(transcribers_per_task=2)
        p.save()
        t = Task(project=p)
        t.save()
        u1 = User.objects.create(username='a', password='a').transcribe_user
        u2 = User.objects.create(username='b', password='b').transcribe_user
        u3 = User.objects.create(username='c', password='c').transcribe_user
        tr1 = 'This is the letter A.'
        tr2 = 'This is the letter B.'
        tr3 = 'This is the letter C.'
        ut1 = UserTask(
            task=t,
            user=u1,
            transcription=tr1,
            status=FINISHED,
            task_type=TRANSCRIPTION,
        )
        ut1.save()
        ut2 = UserTask(
            task=t,
            user=u2,
            transcription=tr2,
            status=FINISHED,
            task_type=TRANSCRIPTION,
        )
        ut2.save()
        ut3 = UserTask(
            task=t,
            user=u3,
            transcription=tr3,
            status=FINISHED,
            task_type=TRANSCRIPTION,
        )
        ut3.save()
        r = t.claim_review(user=u1)
        # transcription must have a root element for the xml parser to
        # be able to read it
        wrapped = '<xml>{}</xml>'.format(r.transcription)
        element = ElementTree.fromstring(wrapped)
        # The first element (not text-only) child of the root element is
        # a span containing the options, for this particular set of
        # transcriptions. There should be only two options. Below is
        # what a correct transcription might look like, after being
        # wrapped with <xml> tags:
        #
        # <xml>
        #     This is the letter
        #     <span class="opts">
        #         <span class="opt firstOption">{A|B|C}.</span>
        #         <span class="opt optHidden">{A|B|C}.</span>
        #     </span>
        # </xml>
        num_alternate_transcriptions = len(element[0])
        self.assertEqual(num_alternate_transcriptions, 2)
