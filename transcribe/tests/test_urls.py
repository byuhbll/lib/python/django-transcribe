from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.test import RequestFactory, TestCase
from django.test.client import Client

from transcribe.models import Project
from transcribe.views.web import DashboardView

User = get_user_model()
CAS_URL = '/accounts/login/?next=/dashboard/'


class URLTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='tester', email='tester@tester.com', password='blah'
        )
        self.inactive_user = User.objects.create_user(
            username='tester2', email='tester2@tester.com', password='blah2'
        )
        self.inactive_user.is_active = False
        self.inactive_user.save()
        self.project = Project.objects.create(
            id=1, title='Test project', description='blah'
        )

    def tearDown(self):
        self.user.delete()
        self.project.delete()

    @staticmethod
    def get_client(auth=False):
        c = Client()
        if auth:
            c.login(username='tester', password='blah')
        return c

    def test_claim_task_no_task(self):
        client = self.get_client(auth=True)
        response = client.get(
            '/project/{}/claim/transcription/'.format(self.project.pk)
        )
        self.assertRedirects(response, '/dashboard/')

    def test_dashboard(self):
        client = self.get_client(True)
        response = client.get('/dashboard/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_dashboard_not_authenticated(self):
        client = self.get_client()
        response = client.get('/dashboard/', follow=True)
        print(response.request)
        print('************************************')
        print(response.redirect_chain)
        self.assertRedirects(response, CAS_URL)

    def test_not_active(self):
        rf = RequestFactory()
        request = rf.get('/dashboard/')
        request.user = self.inactive_user
        with self.assertRaises(PermissionDenied):
            DashboardView.as_view()(request)

    def test_project_view(self):
        client = self.get_client(True)
        response = client.get('/project/1/', follow=True)
        self.assertEqual(response.status_code, 200)
