# Documentation for frontend build process

As an overview: Invoke is used (via `tasks/tasks.py`) to provide a CLI for
doing everything. The static files are managed on the backend by Gulp (via
`tasks/gulpfile.js`), but you generally won't need to use Gulp directly. (You
can, however, and you're also welcome to use the Django dev server directly as
well.)


## Static generation

### CSS

Files are transpiled through SASS, then minified through cssnano.

Any files in `assets/css/lib` will be copied over and will not go through this
transpilation/minification process.


### JS

Files are transpiled through JavaScript, then minified through UglifyJS.

Any files in `assets/js/lib` will be copied over and will not go through this
transpilation/minification process.


## Invoke

Note: these must be executed in the root of the project, where the `tasks/`
directory is.

- `inv run` -- The command you'll probably use most often. Builds the static
files and watches the asset files for any changes (upon which it rebuilds the
appropriate files), and it also starts the Django dev server and the live
reload server.
- `inv sh` -- Runs Django's `shell_plus`.
- `inv shell` -- Runs the default Django shell.
- `inv makemigrations` -- Runs Django's `makemigrations` command. If you need
to do a more custom migration generation, you can use
`inv makemigrations --options "options"` or just use `manage.py` directly.
- `inv manage` -- Runs Django's `manage.py`. You can pass in a command, like
this: `inv manage --command "shell"`. (Or you can just use `manage.py`
directly.)
- `inv migrate` -- Runs Django's `migrate` command. If you need to do a more
custom migration generation, you can use `inv migrate --options "options"` or
just use `manage.py` directly.

### Server

- `inv server.run` -- Starts the Django dev server. It does not start live
reloading.
- `inv server.livereload` -- Starts the Django livereload server.
- `inv server.uwsgi` -- Starts a uwsgi server. It looks for its configs
in `.configs/uwsgi.ini` by default, but you can pass in a path to change that
like this: `inv server.uwsgi --path ./uwsgi.ini`.

### Static

- `inv static.build` -- Cleans all the files in `chaddd/static/` and then builds
the static files using Gulp. (Building here means converting SASS files to CSS,
transpiling JavaScript, and copying everything to the right folders within
`chaddd/static/`.)
- `inv static.watch` -- Builds first (see above) and then watches the `assets/`
folder. If any files there are changed, it rebuilds them and updates
`chaddd/static/`.
- `inv static.clean` -- Removes all the files in `chaddd/static/`.
- `inv static.css` -- Builds the CSS files.
- `inv static.css:watch` -- Builds the CSS files and then watches the
`assets/css` folder for changes. If any files there are changed, it builds the
CSS and updates `chaddd/static/css`.
- `inv static.js` -- Builds the JavaScript files.
- `inv static.js:watch` -- Builds the JavaScript files and then watches the
`assets/js` folder for changes. If any files there are changed, it builds the
JS and updates `chaddd/static/js`.
- `inv static.copy` -- Copies static files over to `chaddd/static`.
- `inv static.copy:watch` -- Copies the static files over and then watches the
specified paths for changes. If any files there are changed, it copies the
files over again (to the specified destination paths).


## Setup

The setup script runs separately from the rest of the Invoke build process,
since things the latter needs (like `configuro`) aren't yet installed.


## Customization

If you need to modify the process for working with static files, you'll want
to edit `tasks/gulpfile.js`, following the guidelines there. You'll also need
to edit `tasks/tasks.py` to add an Invoke wrapper (ala `static_css`, for
example).

If you need to modify the process for something that's not about static files,
you'll want to edit `tasks/tasks.py`, both to add a `@task`-decorated function
and at the bottom of the file to register the task in the default namespace or
one of the collections.
