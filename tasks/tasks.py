#!/usr/bin/env python
"""
Invoke task list for the build process.

==============================================================================

CUSTOMIZATION NOTES

If you modify this file, it's recommended that you put comments around your
changes with "CUSTOM" and an explanation. This'll make it much easier to
upgrade your app to newer build processes later on.

If you're adding new tasks, it's recommended to create a tasks/custom.py file
and then import the custom tasks/collections here. (Again marking changes in
this file with "CUSTOM".)

==============================================================================
"""
import threading

from invoke import Collection, task

from .setup import setup_run

try:
    import configuro
except Exception:
    # This should only happen when running the setup script the first time
    pass

# ----------------------------------------------------------------------------
# Load config.yml to get port

# Load the configs
try:
    config = configuro.load()
except Exception:
    # This should only happen when running the setup script the first time
    config = {}

PORT = config.get('general/port', 8080)


# ----------------------------------------------------------------------------
# Gulp helper

GULPFILEPATH = 'tasks/gulpfile.js'


def _gulp(c, command):
    c.run(
        'gulp -f {} {}'.format(GULPFILEPATH, command),
        pty=True,
        in_stream=False,
    )


# ----------------------------------------------------------------------------
# Server tasks (run, uwsgi)


@task
def server_run(c, port=PORT):
    """
    Runs the Django dev server.
    """
    c.run('python run.py runserver {port}'.format(port=port), pty=True)


@task
def server_livereload(c):
    """
    Runs the Django livereload server. If you're going to run this manually,
    do it in another console.
    """
    c.run(
        'python manage.py livereload config.yml templates/',
        pty=True,
        in_stream=False,
    )


@task
def server_uwsgi(c, path='.configs/uwsgi.ini'):
    """
    Runs the app via a uwsgi server. Expects to find the uwsgi.ini file in
    the .configs/ directory.
    """
    c.run('uwsgi {path}'.format(path=path), pty=True)


# ----------------------------------------------------------------------------
# Static tasks (watch, build, clean)


@task
def static_watch(c):
    """
    Uses Gulp to build the frontend static content and put it in
    chaddd/static, and to keep watching the assets/
    directories for changes. Cleans the static files out first (via Gulp).
    """
    _gulp(c, 'watch')


@task
def static_build(c):
    """
    Uses Gulp to build the frontend static content and put it in
    chaddd/static. Cleans the static files out first
    (via Gulp).
    """
    _gulp(c, 'build')


@task
def static_css(c):
    """
    Uses Gulp to build the CSS.
    """
    _gulp(c, 'css')


@task
def static_css_watch(c):
    """
    Uses Gulp to watch CSS assets and build CSS when anything changes.
    """
    _gulp(c, 'cssWatch')


@task
def static_js(c):
    """
    Uses Gulp to build the JS.
    """
    _gulp(c, 'js')


@task
def static_js_watch(c):
    """
    Uses Gulp to watch JS assets and build JS when anything changes.
    """
    _gulp(c, 'jsWatch')


@task
def static_copy(c):
    """
    Uses Gulp to copy the static files.
    """
    _gulp(c, 'copy')


@task
def static_copy_watch(c):
    """
    Uses Gulp to watch static paths and copy files when anything changes.
    """
    _gulp(c, 'copyWatch')


@task
def static_clean(c):
    """
    Uses Gulp to delete everything in chaddd/static.
    """
    _gulp(c, 'clean')


# ----------------------------------------------------------------------------
# Top-level tasks


@task
def run(c, port=PORT, nostatic=False, nolivereload=True):
    """
    Runs the Django dev server, the Django livereload server, and (if present)
    the Gulp static watchers.

    To run just the dev server: inv run --nostatic --nolivereload
    (This is the same as inv server.run)
    """
    if not nostatic:
        # Run the static file watcher in a separate thread so we can have
        # it run in parallel with the Django dev server
        t = threading.Thread(target=lambda: static_watch(c), daemon=True)
        t.start()

    if not nolivereload:
        # Run the Django livereload server in a separate thread
        # If you don't want this, pass in --livereload false
        t = threading.Thread(target=lambda: server_livereload(c), daemon=True)
        t.start()

    # Start the Django dev server
    server_run(c, port=port)


@task
def manage(c, command):
    """
    Runs ./manage.py with a command.
    """
    c.run('python manage.py {}'.format(command), pty=True)


@task
def refreshdb(c):
    """
    Runs ./manage.py reset_db --noinput && ./manage.py migrate &&
    ./manage.py createsuperuser
    """
    c.run('python manage.py reset_db --noinput', pty=True)
    c.run('python manage.py migrate', pty=True)
    c.run('python manage.py createsuperuser', pty=True)


@task
def makemigrations(c, options=''):
    """
    Runs ./manage.py makemigrations with an optional "options" parameter.
    """
    c.run(
        'python manage.py makemigrations {}'.format(options).strip(), pty=True
    )


@task
def migrate(c, options=''):
    """
    Runs ./manage.py migrate.
    """
    c.run('python manage.py migrate {}'.format(options).strip(), pty=True)


@task
def shell(c):
    """
    Runs ./manage.py shell.
    """
    c.run('python manage.py shell', pty=True)


@task
def sh(c):
    """
    Runs ./manage.py shell_plus.
    """
    c.run('python manage.py shell_plus', pty=True)


# ----------------------------------------------------------------------------

ns = Collection()

ns.add_task(run)
ns.add_task(manage)
ns.add_task(refreshdb)
ns.add_task(makemigrations)
ns.add_task(migrate)
ns.add_task(shell)
ns.add_task(sh)


static = Collection('static')
static.add_task(static_build, 'build')
static.add_task(static_watch, 'watch')
static.add_task(static_clean, 'clean')
static.add_task(static_css, 'css')
static.add_task(static_css_watch, 'css:watch')
static.add_task(static_js, 'js')
static.add_task(static_js_watch, 'js:watch')
static.add_task(static_copy, 'copy')
static.add_task(static_copy_watch, 'copy:watch')
ns.add_collection(static)


server = Collection('server')
server.add_task(server_run, 'run')
server.add_task(server_livereload, 'livereload')
server.add_task(server_uwsgi, 'uwsgi')
ns.add_collection(server)

# Set up the project
ns.add_task(setup_run, 'setup')
