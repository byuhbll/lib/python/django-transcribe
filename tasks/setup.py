#!/usr/bin/env python
"""
Invoke task list for the app setup script. Imported by tasks/tasks.py. And
run by hooks/post_gen_project.py.
"""
import os

from invoke import task

# ----------------------------------------------------------------------------
# Setup task


@task
def setup_run(c):
    print('\nSetting up project...')

    # Check for virtualenv
    c.run(
        'if [[ -z "${VIRTUAL_ENV}" ]]; then echo "A virtualenv needs to be created and activated to run this script."; exit 1; fi'
    )

    # -------------------------------------------------------------------------

    # Python dependencies
    print('Installing Python dependencies...')
    c.run('pip install -U pip')
    c.run('pip install -r dev_requirements.txt')

    # -------------------------------------------------------------------------

    # postactivate
    # print('Linking postactivate and postdeactivate...')
    # c.run(
    #     'ln -nsf "$(pwd)/bin/postactivate.sh" "${VIRTUAL_ENV}/bin/postactivate"'
    # )  # noqa
    # c.run(
    #     'ln -nsf "$(pwd)/bin/postdeactivate.sh" "${VIRTUAL_ENV}/bin/postdeactivate"'
    # )  # noqa

    # -------------------------------------------------------------------------

    # config.yml
    #     config_yml_template = '''# Your local config file. Override if needed.
    #
    # extends: "config.base.yml"
    #
    # bayou:
    #   client_id: "placeholder"
    #   client_secret: "placeholder"
    # '''
    #     print('Setting up config.yml...')
    #     c.run(f'echo "{config_yml_template}" > config.yml')

    # -------------------------------------------------------------------------

    # Only run the following if we're not running tests (i.e., we're a user
    # running the script as part of the cookiecutter hook or running it
    # manually)
    if not os.environ.get('RUNNING_TESTS', False):
        # pyenv
        print('Rehashing pyenv...')
        c.run('pyenv rehash')

        # npm
        print('Installing npm dependencies...')
        c.run('npm install')

        # pre-commit
        print('Setting up pre-commit...')
        c.run('pre-commit install')
        c.run('pre-commit')

    # -------------------------------------------------------------------------

    # Done!
    print('Project ready.')
