from os import chdir, pardir, path

from setuptools import find_packages, setup

VERSION = '0.8.3'
PROJECT_NAME = 'django-transcribe'
PYTHON_SUPPORTED_VERSIONS = ['3.8', '3.9', '3.10', '3.11', '3.12']
DJANGO_SUPPORTED_VERSIONS = ['3.2', '4.1', '4.2']

with open(path.join(path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
chdir(path.normpath(path.join(path.abspath(__file__), pardir)))

req_path = path.join(path.dirname(__file__), 'requirements.txt')
with open(req_path) as req_file:
    REQUIREMENTS = req_file.read().split()

classifiers = [
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
]

classifiers.append('Framework :: Django')
for version in DJANGO_SUPPORTED_VERSIONS:
    classifiers.append('Framework :: Django :: {}'.format(version))

classifiers.append('Programming Language :: Python')
for version in PYTHON_SUPPORTED_VERSIONS:
    classifiers.append('Programming Language :: Python :: {}'.format(version))

packages = find_packages()
excluded_packages = ['transcribe.tests', 'tasks']
packages = [
    p for p in packages if not any(p.startswith(e) for e in excluded_packages)
]

setup(
    name=PROJECT_NAME,
    version=VERSION,
    packages=packages,
    include_package_data=True,
    description='crowd source transcription',
    long_description='Crowd source the transcription of texts, audio, and video.',
    url='https://gitlab.com/byuhbll/lib/python/django-transcribe',
    author='BYU Library Software Engineering',
    author_email='support@lib.byu.edu',
    install_requires=REQUIREMENTS,
    classifiers=classifiers,
)
