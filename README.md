# `django-transcribe`

## Transcribe texts, audio, and video.

Django-Transcribe helps you crowd-source the work of transcription. A typical workflow looks like this:

- Create a new project and upload media (page scans, audio clips, or video clips).
- Visitors grab a task (usually a scanned page) from the project and transcribe it.
- After two visitors have transcribed the same task a reviewer looks at a diff of the two transcriptions, choosing the best options, and making any final changes.
- The reviewer can highlight and tag things like people, places, and dates.
- When all tasks in a project are finsihed it can be downloaded as plain text, html, or an xml document.

![Transcribe screenshot](docs/images/review-and-tagging.png)

## Installing `django-transcribe`

`django-transcribe` is a Django 'app' that is meant to live within a Django project. You can install and use `django-transcribe` within an existing Django project, or create a new Django project. After you've installed `django-transcribe` you can customize it by overriding templates and making other changes at the project level.

To create a new Django project that uses django-transcribe:

Use Python 3.8 or higher.

Install Django 3.2 or higher:

    pip install "django~=3.2"

Install `django-transcribe`:

    pip install django-transcribe

Create a new django project:

    django-admin startproject your_project_name

(in order to avoid naming conflicts don't name your project 'transcribe')

    cd your_project_name

Add 'transcribe' to your INSTALLED_APPS in settings.py:

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'transcribe',
    ]

Add transcribe_urls to your `your_project_name/urls.py` file. Make it look something like this:

    from django.contrib import admin
    from django.urls import include, path
    from transcribe import urls as transcribe_urls
    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('', include(transcribe_urls)),
        path('accounts/', include('django.contrib.auth.urls')),
    ]

    if settings.DEBUG is True:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

Create a media directory. This is where your media files, like scanned images, will be uploaded:

    mkdir media

At the bottom of `your_project_name/settings.py` add:

    MEDIA_ROOT='./media/'
    MEDIA_URL='/media/'

These settings will allow your media files to be served up in your development environment, but for production you will need to configure your webserver to serve files from the media directory.

Run migrations:

    python manage.py migrate

Create a superuser:

    python manage.py createsuperuser

Start the development server:

    python manage.py runserver

Open a browser to the development server url (http://127.0.0.1:8000/).

If everything worked you should be able to login with the username and password you created.

Now you can create a new Transcribe project:
- Click the menu (upper right) and select projects.
- Click Create a New Project.
- Fill out the form including choosing media to upload.
- Direct people to your site and watch as your project gets transcribed!

## Configuration and Customization

In the `your_project_name/settings.py` file you can configure some options:

    TRANSCRIBE_SETTINGS = {
        'support_email': 'transcribe_support@domain.com',
        'from_email': 'no_reply@domain.com',
        'task_expire_days': 14, # how many days until an unfinished task expires
    }

Override template files as you would in any django project.


## Contributing to this project

### Project development setup

Follow these steps to setup your development environment:

1. Clone the repository. Run `git clone https://gitlab.com/byuhbll/lib/python/django-transcribe`.
2. From within the newly created repository directory, create a virtualenv. Run `cd django_transcribe && python -m venv venv` (or create a virtualenv in your favorite way).
3. Activate virtualenv. Run `source venv/bin/activate` (or activate the virtualenv in the way that you like activating virtualenvs).
4. Install dependencies. Run `pip install -r dev_requirements.txt`.
5. Setup pre-commit. Run `pre-commit install`.
6. Contribute!

### Testing in this project

Run `tox` on the command line and the unit tests should run for all supported versions of Python.
