import os
import sys

DEBUG = bool(int(os.environ.get('DEBUG', True)))

try:
    from django.conf import settings

    settings.configure(
        DEBUG=DEBUG,
        USE_TZ=True,
        DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'database.db',
            }
        },
        ALLOWED_HOSTS=('localhost', '127.0.0.1'),
        ROOT_URLCONF=__name__,
        INSTALLED_APPS=[
            'django.contrib.sessions',
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.staticfiles',
            'django.contrib.messages',
            'transcribe',
            'django.contrib.admin',
        ],
        MIDDLEWARE=[
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.common.CommonMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
        ],
        STATIC_URL='/static/',
        MEDIA_ROOT='./media/',
        MEDIA_URL='/media/',
        LOGOUT_REDIRECT_URL='/',
        # SITE_ID=1,
        TEMPLATES=[
            {
                'BACKEND': 'django.template.backends.django.DjangoTemplates',
                'DIRS': [],  # for template dirs not in app_name/templates
                'APP_DIRS': True,
                'OPTIONS': {
                    'debug': DEBUG,
                    'string_if_invalid': '!!missing!!',
                    'context_processors': [
                        # defaults
                        'django.contrib.auth.context_processors.auth',
                        'django.contrib.messages.context_processors.messages',
                        'django.template.context_processors.debug',
                        'django.template.context_processors.i18n',
                        'django.template.context_processors.media',
                        'django.template.context_processors.request',
                        'django.template.context_processors.static',
                        'django.template.context_processors.tz',
                    ],
                },
            }
        ],
        LOGGING={
            'version': 1,
            'disable_existing_loggers': False,
            'handlers': {
                'console': {'level': 'DEBUG', 'class': 'logging.StreamHandler'}
            },
            'loggers': {
                'transcribe': {
                    'handlers': ['console'],
                    'level': 'DEBUG' if DEBUG else 'ERROR',
                }
            },
        },
        GITLAB_ISSUE_CREATION_EMAIL='test@test.test',
        BASE_DIR=os.getcwd(),
        SECRET_KEY='test',
        DEFAULT_AUTO_FIELD='django.db.models.BigAutoField',
    )

    try:
        import django

        setup = django.setup
    except AttributeError:
        pass
    else:
        setup()

except ImportError:
    import traceback

    traceback.print_exc()
    msg = 'To fix this error, run: pip install -r requirements.txt'
    raise ImportError(msg)


from django.contrib import admin  # isort:skip # noqa
from django.urls import include, path  # isort:skip # noqa
from django.conf.urls.static import static  # isort:skip # noqa

from transcribe import urls as transcribe_urls  # isort:skip # noqa

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(transcribe_urls)),
    path('accounts/', include('django.contrib.auth.urls')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if __name__ == '__main__':
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
